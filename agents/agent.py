import numpy as np
import random
from task import Task
from sklearn.neural_network import MLPRegressor
import pip
import copy
import warnings
warnings.filterwarnings("ignore")
try:
    from pyswarm import pso
except:
    # importing Particle Swarm Optimization
    pip.main(['install', "pyswarm"])
    from pyswarm import pso


class Agent():
    def __init__(self, task):
        # Task (environment) information
        self.task = task
        self.state_size = task.state_size
        self.action_size = task.action_size
        self.action_low = task.action_low
        self.action_high = task.action_high
        self.action_range = self.action_high - self.action_low

        self.w = np.random.normal(
            size=(self.state_size, self.action_size),  # weights for simple linear policy: state_space x action_space
            scale=(self.action_range / (2 * self.state_size))) # start producing actions in a decent range
        # Score tracker and learning parameters
        self.best_w = None
        self.best_score = -np.inf
        self.noise_scale = 0.1
        self.discount_rate = 1.#0.2
        self.alpha = 0.1
        self.score_history = []
        self.nextState = None

        # starting the MLPs (one MLP for variable)
        self.currentTrainningState = None
        self.currentAction = None
        self.states_actions_list = []
        self.action_value_list = []
        self.current_action_value_function = None
        
        self.model = MLPRegressor(hidden_layer_sizes=(16,8,4))
        self.model.fit(np.array([[0 for i in range(self.state_size+self.action_size)]]), [.0]) # starting with and initial guess
        self.episode = 0

        # Episode variables
        self.reset_episode()

    def reset_episode(self):
        self.episode += 1
        self.total_reward = 0.0
        self.count = 0
        state = self.task.reset()
        return state

    def step(self, reward, done, next_state):
        # Save experience / reward
        self.total_reward += reward
        self.count += 1
        _, self.action_value_function_next_state = self.getAction(next_state)
        updated_action_function = self.alpha*(reward + (self.discount_rate * self.action_value_function_next_state) - self.current_action_value_function)
        self.action_value_list += [updated_action_function]
        
        # Learn, if at end of episode
        if done:
            print("learning...")
            print("trainingSetSize = ", len(self.states_actions_list), len(self.action_value_list))
            self.learn()
            print("done!")

    def act(self, state):
        # getting the action
        self.currentTrainningState = state
        action, current_action_value_function = self.getAction(state)
        
        # updating attributes
        self.currentAction = action  
        self.current_action_value_function = current_action_value_function
        self.states_actions_list += [np.append(state, action)]
        # returning the action
        return action
    
    def getAction(self, state):
        # optimization considering self.model.predict() as objective function
        action, action_value = pso(self.predict, 
                      [self.action_low for i in range(self.action_size)], 
                      [self.action_high for i in range(self.action_size)], maxiter=5, debug=False)
        action_value = -action_value
        return (action,action_value)
    
    
        
    
    def predict(self, action_input):
        state_input = self.currentTrainningState # state is fixed
        complete_input = np.append(state_input, action_input)
        return -self.model.predict([complete_input]) # it is negative because it represents a maximization problem
    

    def learn(self):
        self.score = self.total_reward / float(self.count) if self.count else 0.0
        self.score_history += [copy.deepcopy(self.score)]
        if self.score > self.best_score:
            self.best_score = self.score
        self.model.fit(self.states_actions_list, self.action_value_list)
        self.states_actions_list = []
        self.action_value_list = []
        